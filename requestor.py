import requests

def get_url():
    return input("Please enter the url you want to request: ")

def get_json():
    n=int(input("How many key-value pair you want to give: "))
    json_data={}
    for i in range(1,n+1):
        key=input(f"Please enter the key{i}: ")
        value=input(f"Please tne the value{i}: ")
        json_data[key]=value
    return json_data

def url_confirmor(url):
    print("$"*10)
    print(url)
    print("$"*10)
    state=input("Please confirm the url entered is correct or not(y/n): ") 
    if state=="y":
        return True
    else:
        return False
        

def json_confirmor(json_data):
    print("$"*10)
    print(json_data)
    print("$"*10)
    state=input("Please confirm the json data entered is correct or not(y/n): ") 
    if state=="y":
        return True
    else:
        return False
        


def get_request():
    url_link=get_url()
    if not url_confirmor(url_link):
        url_link=get_url()
        url_confirmor(url_link)
    output_message=requests.get(url=url_link)
    print("$"*10+" Your output "+"$"*10)
    print(output_message.text)

def post_request():
    url_link=get_url()
    json_data=get_json()
    if not url_confirmor(url_link):
        url_link=get_url()
        url_confirmor(url_link)
    if not json_confirmor(json_data):
        json_data=get_json()
        json_confirmor(json_data)
    output_message=requests.post(url=url_link,json=json_data)
    print("$"*10+" Your output "+"$"*10)
    print(output_message.text)


def put_request():
    url_link=get_url()
    json_data=get_json()
    if not url_confirmor(url_link):
        url_link=get_url()
        url_confirmor(url_link)
    if not json_confirmor(json_data):
        json_data=get_json()
        json_confirmor(json_data)
    output_message=requests.put(url=url_link,json=json_data)
    print("$"*10+" Your output "+"$"*10)
    print(output_message.text)


def delete_request():
    url_link=get_url()
    if not url_confirmor(url_link):
        url_link=get_url()
        url_confirmor(url_link)
    output_message=requests.delete(url=url_link)
    print("$"*10+" Your output "+"$"*10)
    print(output_message.text)


def menu():
    menudata={
            "1":"GET",
            "2":"POST",
            "3":"PUT",
            "4":"DELETE"
            }
    for key, value in menudata.items():
        print(key+" - "+value) 
    option=int(input("Please enter your option(1-4): "))
    function_map={
            1:get_request,
            2:post_request,
            3:put_request,
            4:delete_request
            }
    function_map[option]()


menu()
